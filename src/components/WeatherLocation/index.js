import React from 'react';
import Location from './Location/';
import WeatherData from './WeatherData';

const WeatherLocation = () => (
	<div>
		<Location city={"Río de Janeiro"}></Location>
		<WeatherData></WeatherData>	
		<h5>Weather Location - Web have better views</h5>
		<p>Esto es otra prueba para mostrar componente</p>
	</div>
);

export default WeatherLocation;