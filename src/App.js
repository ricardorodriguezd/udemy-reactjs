import React, { Component } from 'react';
import WeatherLocation from './components/WeatherLocation/';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
            Learn React
          </a>

          <WeatherLocation></WeatherLocation>
          
        </header>
      </div>
    );
  }
}

export default App;
